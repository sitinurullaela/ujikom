

CREATE TABLE `admin` (
  `id_admin` int(12) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama_admin` varchar(10) NOT NULL,
  `id_level` int(12) NOT NULL,
  PRIMARY KEY (`id_admin`),
  KEY `id_level` (`id_level`),
  KEY `id_level_2` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO admin VALUES("1","admin1","e00cf25ad42683b3df678c61f42c6bda","nurul","1");





CREATE TABLE `level` (
  `id_level` int(12) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(25) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","pelanggan"),
("2","admin");





CREATE TABLE `pelanggan` (
  `id_pelanggan` char(12) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nomor_kwh` varchar(20) NOT NULL,
  `nama_pelanggan` varchar(20) NOT NULL,
  `saldo` int(10) NOT NULL,
  `alamat` text NOT NULL,
  `id_tarif` int(12) NOT NULL,
  PRIMARY KEY (`id_pelanggan`),
  KEY `id_tarif` (`id_tarif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pelanggan VALUES("1","laela","d9a34dadae0e6cb99cdd778c178ae7e5","112","laela","0","loji","1"),
("20190407001","mega6","c87eccfde24d6d0c4367a472a0ee06eb","145","mega","500000","loji","1"),
("20190409004","shela7","c688c36df2d6c25dba201159f45d0e9f","18289446","shela","1715000","laladon","1");





CREATE TABLE `pembayaran` (
  `id_pembayaran` char(12) NOT NULL,
  `id_tagihan` char(12) NOT NULL,
  `id_pelanggan` char(12) NOT NULL,
  `tanggal_pembayaran` datetime NOT NULL,
  `bulan_bayar` varchar(25) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `biaya_admin` int(25) NOT NULL,
  `denda` int(10) NOT NULL,
  `total_bayar` int(25) NOT NULL,
  `id_admin` varchar(15) NOT NULL,
  PRIMARY KEY (`id_pembayaran`),
  KEY `id_tagihan` (`id_tagihan`,`id_pelanggan`,`id_admin`),
  KEY `id_admin` (`id_admin`),
  KEY `id_pelanggan` (`id_pelanggan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pembayaran VALUES("20181219001","20181219001","20190409004","2018-12-19 09:46:59","12","10000","2500","0","12500","nurul"),
("20181219002","20181219007","20190409004","2018-12-19 10:06:09","12","10000","2500","0","12500","nurul");





CREATE TABLE `penggunaan` (
  `id_penggunaan` char(12) NOT NULL,
  `id_pelanggan` char(12) NOT NULL,
  `bulan` int(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `meter_awal` varchar(15) NOT NULL,
  `meter_akhir` varchar(15) NOT NULL,
  PRIMARY KEY (`id_penggunaan`),
  KEY `id_pelanggan` (`id_pelanggan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO penggunaan VALUES("20181219001","20190409004","12","2018","10","60"),
("20190409001","20190409004","4","2019","0","10");





CREATE TABLE `saldo` (
  `id_saldo` varchar(12) NOT NULL,
  `username` varchar(20) NOT NULL,
  `jumlah_isi` int(10) NOT NULL,
  `metode` varchar(30) NOT NULL,
  `tanggal_pembayaran` datetime NOT NULL,
  `status` varchar(40) NOT NULL,
  PRIMARY KEY (`id_saldo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO saldo VALUES("20181219001","shela7","1000000","Bank BRI","2018-12-19 11:35:47","TELAH DIVERIFIKASI"),
("20190409002","mega6","500000","Bank BRI","2019-04-09 08:15:44","TELAH DIVERIFIKASI"),
("20190409003","shela7","1000000","Bank BRI","2019-04-09 08:45:23","TELAH DIVERIFIKASI");





CREATE TABLE `tagihan` (
  `id_tagihan` char(12) NOT NULL,
  `id_penggunaan` char(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `bulan` int(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `jumlah_meter` varchar(25) NOT NULL,
  `status` varchar(40) NOT NULL,
  PRIMARY KEY (`id_tagihan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO tagihan VALUES("20181219001","20190409001","20190409004","4","2019","10","Lunas"),
("20181219002","20190409001","20190409004","4","2019","10","Belum Dibayar"),
("20181219003","20190409001","20190409004","4","2019","10","Belum Dibayar"),
("20181219004","20190409001","20190409004","4","2019","10","Belum Dibayar"),
("20181219005","20190409001","20190409004","4","2019","10","Belum Dibayar"),
("20181219006","20190409001","20190409004","4","2019","10","Belum Dibayar"),
("20181219007","20190409001","20190409004","4","2019","10","Lunas"),
("20190409001","20190409001","20190409004","4","2019","10","Lunas");





CREATE TABLE `tarif` (
  `id_tarif` int(12) NOT NULL AUTO_INCREMENT,
  `daya` varchar(5) NOT NULL,
  `tarifperkwh` int(6) NOT NULL,
  PRIMARY KEY (`id_tarif`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO tarif VALUES("1","400","1000");



