<!DOCTYPE html>
<?php
include "koneksi.php";
session_start();
 $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM admin where username='$_SESSION[username]'");
  $admin = mysqli_fetch_array($query_pelanggan);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Halaman Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="../img/logopln.png" />
</head>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html"><img src="../img/pln1.png" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="index.html"><img src="../img/logopln.png" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <div class="search-field d-none d-md-block">
          <form class="d-flex align-items-center h-100" action="#">
            <div class="input-group">
              <div class="input-group-prepend bg-transparent">
                  <i class="input-group-text border-0 mdi mdi-magnify"></i>                
              </div>
              <input type="text" class="form-control bg-transparent border-0" placeholder="Search projects">
            </div>
          </form>
        </div>
        <ul class="navbar-nav navbar-nav-right">
          
          <li class="nav-item d-none d-lg-block full-screen-link">
            <a class="nav-link">
              <i class="mdi mdi-fullscreen" id="fullscreen-button"></i>
            </a>
          </li>
          <li class="nav-item nav-logout d-none d-lg-block">
            <a class="nav-link" href="keluar.php">
              <i class="mdi mdi-logout mr-2 text-primary"></i>
            </a>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>

    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
              <div class="nav-profile-image">
                <img src="images/faces/face1.jpg" alt="profile">
                <span class="login-status online"></span> <!--change to offline or busy as needed-->              
              </div>
              <div class="nav-profile-text d-flex flex-column">
                <span class="font-weight-bold mb-2">Admin</span>
              </div>
              <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#general-pages" aria-expanded="false" aria-controls="general-pages">
              <span class="menu-title">Kelola Data</span>
              <i class="menu-arrow"></i>
              <i class="mdi mdi-table-large menu-icon"></i>
            </a>
            <div class="collapse" id="general-pages">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="index.php">Data Pelanggan</a></li>
                <li class="nav-item"> <a class="nav-link" href="data_pembayaran.php">Data Pembayaran</a></li>
                <li class="nav-item"> <a class="nav-link" href="data_saldo.php">Data tagihan</a></li>
                <li class="nav-item"> <a class="nav-link" href="data_saldo.php">Data penggunaan</a></li>
                <li class="nav-item"> <a class="nav-link" href="data_saldo.php">Data saldo</a></li>
              </ul>
              </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="verifikasi.php">
              <span class="menu-title">Verifikasi</span>
              <i class="mdi mdi-format-list-bulleted menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="Backup.php">
              <span class="menu-title">Backup</span>
              <i class="mdi mdi-format-list-bulleted menu-icon"></i>
            </a>
          </li>
          
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          
          <div class="page-header">
            <h3 class="page-title">
              <span class="page -title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>                 
              </span>
              Dashboard
            </h3>
      
          </div>
          <?php
    $id_saldo = $_GET['id_saldo'];
    $pilih = mysqli_query($koneksi, "SELECT * FROM saldo WHERE id_saldo='$id_saldo'");
    $data = mysqli_fetch_array($pilih);
    
    ?>
         <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Verifikasi Data</h4>
                  <form class="forms-sample" action="" method="POST">
                    <div class="form-group">
                      <label class="col-sm-3 col-form-label">Username</label>
                      <input type="text" class="form-control" name="username" value="<?php echo $data['username']; ?>" readonly>
                    </div>
                     <?php
                      $query_user = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE username='$data[username]'");
                      $user = mysqli_fetch_array($query_user);
                      ?>
                    <div class="form-group">
                      <label >Nama Pelanggan</label>
                      <input type="text" name="nama_pelanggan" class="form-control" value="<?php echo $user['nama_pelanggan']; ?>" readonly">
                    </div>
                    <div class="form-group">
                      <label >Saldo</label>
                      <input type="text" name="jumlah_isi" class="form-control" value="<?php echo $data['jumlah_isi']; ?>" readonly>
                    </div>
                    
                    <button type="submit" name="verif" class="btn btn-gradient-primary mr-2">Verifikasi</button>
                  </form>
                </div>
              </div>
            </div>

            <?php
      if (isset($_POST['verif'])){
        $username = $_POST['username'];
        $nama_pelanggan = $_POST['nama_pelanggan'];
        $jumlah_isi = $_POST['jumlah_isi'];
        if ($data['status'] == "TELAH DIVERIFIKASI") {
          echo "<script>window.alert('Status telah diverifikasi')
          window.location='verifikasi.php'</script>";
        } else {
        $saldo = mysqli_query($koneksi,"UPDATE saldo SET username = '$username', jumlah_isi = '$jumlah_isi', status ='TELAH DIVERIFIKASI' WHERE id_saldo='$id_saldo'");
        $pelanggan = mysqli_query($koneksi,"UPDATE pelanggan SET saldo = saldo+'$jumlah_isi' WHERE username='$data[username]'");
        if($saldo AND $pelanggan){
          /*echo "<script>window.alert('Data Berhasil DiVerifikasi')
          window.location='verifikasi.php'</script>";*/
        }else{
          //echo "Gagal";
        }
      } 
    }
      ?>

          
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="https://www.bootstrapdash.com/" target="_blank">NurPLN</a>. </span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <script src="vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
   <script scr="../js/jquery.dataTables.min.js"></script>
  <script>
    $(document).ready(function(){
      $('#example').DataTable();
    });
  </script>
  <script src="js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="js/dashboard.js"></script>
  <!-- End custom js for this page-->
</body>
</html>