<?php 
include 'koneksi.php';
session_start();
if (!isset($_SESSION['username'])) {
  header('location:../pelanggan/login.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Halaman Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="../img/logopln.png" />
</head>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html"><img src="../img/pln1.png" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="index.html"><img src="../img/logopln.png" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <div class="search-field d-none d-md-block">
          <form class="d-flex align-items-center h-100" action="#">
            <div class="input-group">
              <div class="input-group-prepend bg-transparent">
                  <i class="input-group-text border-0 mdi mdi-magnify"></i>                
              </div>
              <input type="text" class="form-control bg-transparent border-0" placeholder="Search projects">
            </div>
          </form>
        </div>
        <ul class="navbar-nav navbar-nav-right">
          
          <li class="nav-item d-none d-lg-block full-screen-link">
            <a class="nav-link">
              <i class="mdi mdi-fullscreen" id="fullscreen-button"></i>
            </a>
          </li>
          <li class="nav-item nav-logout d-none d-lg-block">
            <a class="nav-link" href="keluar.php">
              <i class="mdi mdi-logout mr-2 text-primary"></i>
            </a>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>

    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
              <div class="nav-profile-image">
                <img src="images/faces/face1.jpg" alt="profile">
                <span class="login-status online"></span> <!--change to offline or busy as needed-->              
              </div>
              <div class="nav-profile-text d-flex flex-column">
                <span class="font-weight-bold mb-2">Admin</span>
              </div>
              <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#general-pages" aria-expanded="false" aria-controls="general-pages">
              <span class="menu-title">Kelola Data</span>
              <i class="menu-arrow"></i>
              <i class="mdi mdi-table-large menu-icon"></i>
            </a>
            <div class="collapse" id="general-pages">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="index.php">Data Pelanggan</a></li>
                <li class="nav-item"> <a class="nav-link" href="data_pembayaran.php">Data Pembayaran</a></li>
                <li class="nav-item"> <a class="nav-link" href="data_tagihan.php">Data tagihan</a></li>
                <li class="nav-item"> <a class="nav-link" href="data_penggunaan.php">Data Penggunaan</a></li>
                <li class="nav-item"> <a class="nav-link" href="data_saldo.php">Data Saldo</a></li>
              </ul>
              </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="verifikasi.php">
              <span class="menu-title">Verifikasi</span>
              <i class="mdi mdi-format-list-bulleted menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="Backup.php">
              <span class="menu-title">Backup</span>
              <i class="mdi mdi-folder-download menu-icon"></i>
            </a>
          </li>
          
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          
          <div class="page-header">
            <h3 class="page-title">
              <span class="page -title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>                 
              </span>
              Dashboard
            </h3>
      
          </div>
          <div class="row">
         <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Data Pelanggan</h4>
                  <table class="table" id="example">
                    <thead>
                      <tr>
                      <th>No</th>
                        <th>ID pelanggan</th>
                        <th>Tanggal bayar</th>
                        <th>Bulan</th>
                        <th>Jumlah</th>
                        <th>Biaya admin</th>
                        <th>Total</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <?php
                    include "koneksi.php";
                    $No=1;
                    $pilih=mysqli_query($koneksi,"SELECT * FROM pembayaran");
                    while($pembayaran=mysqli_fetch_array($pilih))
                    {
                    ?>
                    <tbody>
                      <tr>
                      <td><?php echo $No++;?></td>
                       <td><?php echo $pembayaran['id_pelanggan'];?></td>
                       <td><?php echo $pembayaran['tanggal_pembayaran'];?></td>
                       <td><?php echo $pembayaran['bulan_bayar'];?></td>
                       <td><?php echo $pembayaran['jumlah_bayar'];?></td>
                       <td><?php echo $pembayaran['biaya_admin'];?></td>
                       <td><?php echo $pembayaran['total_bayar'];?></td>
                      		<td>
                        <a href="hapus_pembayaran.php?id_pembayaran=<?=$pembayaran['id_pembayaran'];?>">
                            <button type="button" class="btn btn-gradient-danger btn-icon"><i class="mdi mdi-delete"></i>
                        </button></a></td>
                       </tr>
                      </tr>
                    </tbody>
                    <?php
                }
                ?>
                  </table>
                  <div class="center">
                            <button type="button" class="btn btn-gradient-info btn-fw" onClick="print_d()"><i class="mdi mdi-printer btn-icon-append">Print Document</i>
                        </button>
                        </div>
              </div>
            </div>
            </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">NurPLN</a>. </span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <script src="vendors/js/vendor.bundle.addons.js"></script>
  <script>
  function print_d(){
  	window.open("printp.php","_blank");
  }
  </script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/jquery.dataTables.min.js"></script>
  <script>
    $(document).ready(function(){
      $('#example').DataTable();
    });
  </script>
  <script src="js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="js/dashboard.js"></script>
  <!-- End custom js for this page-->
</body>
</html>
