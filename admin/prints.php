<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Penggunaan</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/style.css">
  <!-- endinject -->
</head>
<body>
     
          <div class="row">
         <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Data Penggunan</h4>
                  <table class="table table-bordered" >
                    <thead>
                      <tr>
                        <th>NO</th>
                        <th>ID Saldo</th>
                        <th>Username</th>
                        <th>Jumlah isi</th>
                        <th>Metode</th>
                        <th>Tanggal pembayaran</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <?php
                    include "koneksi.php";
                    $No=1;
                    $pilih=mysqli_query($koneksi,"SELECT * FROM saldo");
                    while($saldo=mysqli_fetch_array($pilih))
                    {
                    ?>
                    <tbody>
                      <tr>
                      <td><?php echo $No++;?></td>
                        <td><?php echo $saldo['id_saldo'];?></td>
                        <td><?php echo $saldo['username'];?></td>
                        <td><?php echo $saldo['jumlah_isi'];?></td>
                        <td><?php echo $saldo['metode'];?></td>
                        <td><?php echo $saldo['tanggal_pembayaran'];?></td>
                        <td><?php echo $saldo['status'];?></td>
                       </tr>
                      </tr>
                    </tbody>
                    <?php
                }
                ?>
                  </table>
                 
           </div>
           </div>
           </div>
           </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <script src="vendors/js/vendor.bundle.addons.js"></script>
  <script>
  		window.load = print_d();
  		function print_d(){
  			window.print();
  		}
  </script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/jquery.dataTables.min.js"></script>
  <script>
    $(document).ready(function(){
      $('#example').DataTable();
    });
  </script>
  <script src="js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="js/dashboard.js"></script>
  <!-- End custom js for this page-->
</body>
</html>