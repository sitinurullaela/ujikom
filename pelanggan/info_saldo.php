<?php
  include "koneksi.php";
  session_start();

  if(empty($_SESSION['username']))
{
	header("location:login.php");
}else{
          $query_pelanggan=mysqli_query($koneksi,"SELECT * FROM pelanggan WHERE username='$_SESSION[username]'");
          $pelanggan= mysqli_fetch_array($query_pelanggan);
          }
          ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Halaman User</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="../img/logopln.png" />
</head>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html"><img src="../img/pln1.png" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="index.html"><img src="../img/logopln.png" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <div class="search-field d-none d-md-block">
          <form class="d-flex align-items-center h-100" action="#">
            <div class="input-group">
              <div class="input-group-prepend bg-transparent">
                  <i class="input-group-text border-0 mdi mdi-magnify"></i>                
              </div>
              <input type="text" class="form-control bg-transparent border-0" placeholder="Search projects">
            </div>
          </form>
        </div>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item d-none d-lg-block full-screen-link">
            <a class="nav-link">
              <i class="mdi mdi-fullscreen" id="fullscreen-button"></i>
            </a>
          </li>
          <li class="nav-item nav-logout d-none d-lg-block">
            <a class="nav-link" href="keluar.php">
              <i class="mdi mdi-logout mr-2 text-primary"></i>
            </a>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
              <div class="nav-profile-image">
                <img src="images/faces/face1.jpg" alt="profile">
                <span class="login-status online"></span> <!--change to offline or busy as needed-->              
              </div>
              <div class="nav-profile-text d-flex flex-column">
                <span class="font-weight-bold mb-2">user</span>
              </div>
              <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php">
              <span class="menu-title"> Home </span>
              <i class="mdi mdi-home menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#general-pages" aria-expanded="false" aria-controls="general-pages">
              <span class="menu-title">Riwayat</span>
              <i class="menu-arrow"></i>
              <i class="mdi mdi-table-large menu-icon"></i>
            </a>
            <div class="collapse" id="general-pages">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="data_penggunaan.php">Riwayat Penggunaan </a></li>
                <li class="nav-item"> <a class="nav-link" href="data_pembayaran.php"> Riwayat pembayaran </a></li>
              

              </ul>
              </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="saldo.php">
              <span class="menu-title">saldo</span>
              <i class="mdi mdi-format-list-bulleted menu-icon"></i>
            </a>
          </li>
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          
          <div class="page-header">
            <h3 class="page-title">
              <span class="page -title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>                 
              </span>
              Dashboard
            </h3>
          </div>
          <?php
          include "koneksi.php";
          $query_saldo = mysqli_query($koneksi, "SELECT * FROM saldo WHERE username='$_SESSION[username]' and status='Menunggu Verifikasi'");
          $saldo = mysqli_fetch_array($query_saldo);{
          ?>
          <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Saldo</h4>
                  <form class="forms-sample" action="proses_saldo.php" method="POST">
                    <div class="form-group">
                      <label for="exampleInputUsername1">Username</label>
                      <input type="text" class="form-control" id="exampleInputUsername1" placeholder="Username" value="<?php echo $_SESSION['username'];?>" autocomplete="off" disabled>
                    </div>
                    <div class="form-group">
                      <label>Metode Pembayaran</label>
                      <input type="text" class="form-control" value="<?php echo $saldo['metode'];?>" placeholder="Isi Saldo" disabled>
                    </div>
                    <div class="form-group">
                      <label >Tanggal Top-Up</label>
                      <input type="text" class="form-control" name="username" placeholder="Username" autocomplete="off" required="" value="<?php echo $saldo['tanggal_pembayaran'];?>" disabled>
                    </div>
                    <div class="form-group">
                      <label >Nomor Rekening</label>
                      <input type="text" class="form-control" value="2314628010" placeholder="Isi Saldo" disabled>
                    </div>
                    <div class="form-group">
                      <label >Atas Nama</label>
                    <input type="text" class="form-control" value="Siti Nurul Laela Afifah " placeholder="Isi Saldo" disabled>
                    </div>
                    <div class="form-group">
                      <label >Jumlah yang harus dibayar</label>
                    <input type="text" class="form-control" value="<?php echo $saldo['jumlah_isi']; ?>" placeholder="Isi Saldo" disabled>
                    </div>
                    <div class="form-group">
                      <label >status</label>
                    <input type="text" class="form-control" value="<?php echo $saldo['status'];?>" placeholder="Isi Saldo" disabled>
                    </div> 
                  </form>
                </div>
              </div>
            </div>
        </div>
        </div>
        <?php
      }
      ?>
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap Dash</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
          </div>
        </footer>
      </div>
    </div>
  </div>
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <script src="vendors/js/vendor.bundle.addons.js"></script>
  <script src="js/off-canvas.js"></script>
   <script scr="../js/jquery.dataTables.min.js"></script>
  <script>
    $(document).ready(function(){
      $('#example').DataTable();
    });
  </script>
  <script src="js/misc.js"></script>
  <script src="js/dashboard.js"></script>
</body>
</html>