<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Cetak struk</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/style.css">
  <!-- endinject -->
</head>
<body>
     
          <div class="row">
          <div class="container">
          <table border="0" style="text-align:left">
          <?php
          include "koneksi.php";
          $query_pelanggan = mysqli_query($koneksi,"SELECT * FROM pelanggan");
          $pelanggan=mysqli_fetch_array($query_pelanggan);
         ?>
         <tr>
         <td> ID Pelanggan </td><td> : </td><td><?php echo $pelanggan['id_pelanggan'];?></td>
         </tr>
         <tr>
         <td> Nama Pelanggan</td><td> : </td><td><?php echo $pelanggan['nama_pelanggan'];?></td>
         </tr>	
          <tr>
         <td> Nomor Kwh</td><td> : </td><td><?php echo $pelanggan['nomor_kwh'];?></td>
         </tr>
          <tr>
         <td> Alamat</td><td> : </td><td><?php echo $pelanggan['alamat'];?></td>
         </tr>
          <?php
          include "koneksi.php";
          $query_pembayaran=mysqli_query($koneksi,"SELECT * FROM pembayaran");
          $pembayaran=mysqli_fetch_array($query_pembayaran);
          ?>
         
          </table></div></div>

         
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <script src="vendors/js/vendor.bundle.addons.js"></script>
  <script>
  		window.load = print_d();
  		function print_d(){
  			window.print();
  		}
  </script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/jquery.dataTables.min.js"></script>
  <script>
    $(document).ready(function(){
      $('#example').DataTable();
    });
  </script>
  <script src="js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="js/dashboard.js"></script>
  <!-- End custom js for this page-->
</body>
</html>
