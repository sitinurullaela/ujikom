<!DOCTYPE html>
<html>
<head>
	<title>Halaman Login</title>
	<link rel="stylesheet" type="text/css" href="stylelogin.css">
</head>
<body>
 
	<div class="kotak_login">
		<p class="tulisan_login">Register</p>
 
		<form action="proses_regis.php" method="post">
			<label>Username</label>
			<input type="text" name="username" class="form_login" placeholder="Username" autocomplete="off" required>
			<label>Password</label>
			<input type="password" name="password" class="form_login" placeholder="Password " autocomplete="off" required>
			<label>Nama Pelanggan</label><input type="text" name="nama_pelanggan" class="form_login" placeholder="Username" autocomplete="off" required>
			<label>Nomor Kwh</label>
			<input type="number" name="nomor_kwh" class="form_login" placeholder="Nomor Kwh" autocomplete="off" required>
			<label>Alamat</label>
			<input type="text" name="alamat" class="form_login" placeholder="alamat" autocomplete="off" required>
			<div class="form-group row"> <label>ID Tarif</label>
            <select class="custom-select" name="id_tarif" style="width:100% ">
              <?php 
                    include 'koneksi.php';
                    $query_tarif = mysqli_query($koneksi, "SELECT * FROM tarif");
                    while ($tarif = mysqli_fetch_array($query_tarif)) { ?>
                    <option value="<?php echo $tarif['id_tarif']; ?>"><?php echo $tarif['daya']," watt-",$tarif['tarifperkwh'],"perkwh"; ?></option>
                  <?php
                    }
                  ?> 
            </select>
    </div>
    <br/>
 
			<input type="submit" name="simpan" class="tombol_login" value="Simpan">
 
			<br/>
			<br/>
			
		</form>
		<h4>Sudah Memiliki Akun <a href="login.php">Login</a></h4>
		
	</div>	

 
 
</body>
</html>