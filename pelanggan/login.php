<!DOCTYPE html>
<html>
<head>
	<title>Halaman Login</title>
	<link rel="stylesheet" type="text/css" href="stylelogin.css">
</head>
<body>
 
	<div class="kotak_login">
		<p class="tulisan_login">Silahkan login</p>
 
		<form action="cek_login.php" method="post">
			<label>Username</label>
			<input type="text" name="username" class="form_login" placeholder="Username" autocomplete="off" required="required">
 
			<label>Password</label>
			<input type="password" name="password" class="form_login" placeholder="Password " autocomplete="off" required="required">
 
			<input type="submit" name="submit" class="tombol_login" value="LOGIN">
 
			<br/>
			<br/>
			<center>
				<p>Belum memiliki akun? <a href="daftar.php" >Daftar</a></p>

			<br/><a href="help.php">Help</a>
			</center>
		</form>
		
	</div>	

 
 
</body>
</html>